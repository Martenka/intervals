import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.scss'],
  template: `
    <div>{{ selectedString }}</div>
    <div>{{ selectedNote }}</div>
    <div>{{ selectedInterval }}</div>
    <button (click)="generate()">Generate</button>
  `
})
export class AppComponent {
  title = 'intervals';
  notes = ['a', 'a#', 'b', 'c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#'];
  intervals = ['P1', 'm2', 'M2', 'm3', 'M3', 'P4', 'P5', 'm6', 'M6', 'm7', 'M7', 'P8'];
  strings = ['H', 'E', 'A', 'D', 'G'];
  distance = [];

  selectedString = 'E';
  selectedNote = 'a';
  selectedInterval = 'P1';

  generate(): void {
    this.selectedNote = this.notes[this.randomInt(0, 12)];
  }

  randomInt(min: number, max: number): number {
    const rand = Math.random();
    return Math.floor(rand * max) + min;
  }
}
